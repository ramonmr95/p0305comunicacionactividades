package net.iescierva.ramonmr95.p0305comunicacionactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ConfirmacionActivity extends AppCompatActivity {

    private TextView nombreUsuarioTextView;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion);

        nombreUsuarioTextView = findViewById(R.id.nombreUsuarioTextView);

        try {
            Bundle extras = getIntent().getExtras();
            String nombre = extras.getString("nombre");
            nombreUsuarioTextView.setText("Hola " + nombre + ", ¿Aceptas las condiciones?");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void accion(View view) {
        intent = new Intent(this, MainActivity.class);

        if (view.getId() == R.id.aceptarButton) {
            intent.putExtra("resultado", "Aceptado");
            setResult(RESULT_OK, intent);
        }
        else if (view.getId() == R.id.cancelarButton) {
            intent.putExtra("resultado", "Rechazado");
            setResult(RESULT_CANCELED, intent);
        }
        finish();

    }
}
