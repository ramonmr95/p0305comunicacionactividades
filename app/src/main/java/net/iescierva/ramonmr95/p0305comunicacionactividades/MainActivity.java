package net.iescierva.ramonmr95.p0305comunicacionactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText nombreEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombreEditText = findViewById(R.id.nombreEditText);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1234) {
            String res = data.getExtras().getString("resultado");
            TextView resultadoTextView = findViewById(R.id.resultadoTextView);
            resultadoTextView.setText("Resultado: " + res);
        }
    }

    public void lanzarConfirmacion(View view) {
        Intent intent = new Intent(this, ConfirmacionActivity.class);
        intent.putExtra("nombre", nombreEditText.getText().toString());
        startActivityForResult(intent, 1234);
    }
}

